
%% Clear workspace
% clear all
close all
% clc


%% Plot 1 - plot the feasible region point by point

% Clarke transformation
K = 2/3*[   1,       -1/2,       -1/2;
            0,  sqrt(3)/2, -sqrt(3)/2;
          1/2,        1/2,        1/2];

% Phase current limit
Im = 3.67;

% Grid the phase current space
[ia, ib] = meshgrid(-Im:.1:Im);
ia = ia(:)';
ib = ib(:)';
ic = -ia - ib;

% Remove points that have |ic|>Im
indices = find(abs(ic) > Im);
ia(indices) = [];
ib(indices) = [];
ic(indices) = [];

% Create vector i_abc
i_abc = [ia; ib; ic];

% Transform to alpha-beta
i_abg = K*i_abc;

% Plot the the alpha-beta points
figure;
hold on
axis equal
plot(i_abg(1, :), i_abg(2, :), '.');

% Plot inscribed circle
ang = 0:0.01:2*pi; 
x_circle = Im*cos(ang);
y_circle = Im*sin(ang);
plot(x_circle, y_circle);


%% Plot 2 - plot the borders of feasible region using analytical formulas
f1 = @(x) sqrt(3)/3*x + 2*sqrt(3)/3*Im;
f2 = @(x) sqrt(3)/3*x - 2*sqrt(3)/3*Im;
f3 = @(x) -sqrt(3)/3*x + 2*sqrt(3)/3*Im;
f4 = @(x) -sqrt(3)/3*x - 2*sqrt(3)/3*Im;

i_alpha_1 = -Im;
i_alpha_2 = 0;
i_alpha_3 = +Im;

i_beta_1 = -1*sqrt(3)/3*Im;
i_beta_2 = -2*sqrt(3)/3*Im;
i_beta_3 = +sqrt(3)/3*Im;
i_beta_4 = +2*sqrt(3)/3*Im;

line1 = [i_alpha_1, i_alpha_1;
          i_beta_1, i_beta_3];
line2 = [     i_alpha_1, i_alpha_2;
          f1(i_alpha_1), f1(i_alpha_2)];
line3 = [     i_alpha_2, i_alpha_3;
          f3(i_alpha_2), f3(i_alpha_3)];
line4 = [i_alpha_3, i_alpha_3;
          i_beta_3, i_beta_1];
line5 = [     i_alpha_3, i_alpha_2;
          f2(i_alpha_3), f2(i_alpha_2)];
line6 = [     i_alpha_2, i_alpha_1;
          f4(i_alpha_2), f4(i_alpha_1)];
border = [line1, line2, line3, line4, line5, line6];

%figure;
hold on
axis equal

% Plot borders
% plot(line1(1, :), line1(2, :));
% plot(line2(1, :), line2(2, :))
% plot(line3(1, :), line3(2, :))
% plot(line4(1, :), line4(2, :))
plot(border(1, :), border(2, :), 'g');
plot(x_circle, y_circle);


%% Plot 3 - plot the feasible region using other form of the constraints (to check validity)
f1 = @(x, y) abs(x);
f2 = @(x, y) abs(x + sqrt(3)*y);
f3 = @(x, y) abs(x - sqrt(3)*y);

% Phase current limit
Im = 3.67;

% Grid the DQ current space
[ia, ib] = meshgrid(-1.5*Im:.05:1.5*Im);
ia = ia(:)';
ib = ib(:)';

% Remove points that do not satisfy the conditions
indices = [];
indices = [indices, find(f1(ia, ib) > Im)];
indices = [indices, find(f2(ia, ib) > 2*Im)];
indices = [indices, find(f3(ia, ib) > 2*Im)];
indices = unique(indices);
ia(indices) = [];
ib(indices) = [];


%figure;
hold on
axis equal

% Plot borders
% plot(line1(1, :), line1(2, :));
% plot(line2(1, :), line2(2, :))
% plot(line3(1, :), line3(2, :))
% plot(line4(1, :), line4(2, :))
plot(ia, ib, '.');
plot(x_circle, y_circle);


%% Plot 4 - filled feasible region
hfig = figure;
set_figure();
hold on
axis equal
patch('Faces', 1:length(border), 'Vertices', border', 'EdgeColor', 'g', 'FaceColor', 'g', 'FaceAlpha', 0.1);
plot(x_circle, y_circle, '--b');
hleg = legend('Feasible region$\quad\quad\,\,$', '$i_{\alpha}^2 + i_{\beta}^2 = I_{\textrm{max}}^2$', 'Location', 'northeastoutside');
xlabel('$i_{\alpha}$ [A]');
ylabel('$i_{\beta}$ [A]');

% Enlarge figure width by the width of the legend box
enlarge_fig(hfig, hleg);

% Set x-axis limits
ylim([-4.5, 4.5])

% Set numbers on axes
set(gca,'xtick', [-6:2:6]);
set(gca,'ytick', [-6:2:6]);


%% Save plot 4

% Save
% file_name = 'constraints1.pdf';
% saveas(gca, file_name);
% system(['pdfcrop ', file_name, ' ', file_name]);


%% Save to border_current_alpha_beta
border_current_ab = border;
circle_x_current_ab = x_circle;
circle_y_current_ab = y_circle;




