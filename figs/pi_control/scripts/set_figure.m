function set_figure()
% This function sets figure according to my preferences.
% Created by cernylu6.


% Set latex interpreter
set(gcf, 'defaultTextInterpreter', 'latex');
set(gcf, 'defaultLegendInterpreter', 'latex');
set(gcf, 'defaultAxesTickLabelInterpreter', 'latex');
set(gcf, 'defaultAxesTickLabelInterpreter', 'latex');
set(gca, 'TickLabelInterpreter', 'latex');

% Set fontsize
set(gca, 'Fontsize', 12);

% Set size of the figure
screen_size = get(0, 'ScreenSize');
screen_aspect_ratio = screen_size(3)/screen_size(4);
% golden_ratio = 1.61803398875;
%ratio = 1.414; % A4 paper ratio
ratio = 1;
height = 0.4;
width = height*ratio/screen_aspect_ratio;
set(gcf, 'units', 'normalized');
set(gcf, 'position', [(1-width)/2, (1-height)/2, width, height]);

% Add grid
grid on

% Add minor grid
% grid minor

% Add box
box on


end
