
%% Clear workspace
% clear all
close all
clc

%% Plot 1 - plot the feasible region point by point

% Clarke transformation
K = 2/3*[   1,       -1/2,       -1/2;
            0,  sqrt(3)/2, -sqrt(3)/2;
          1/2,        1/2,        1/2];

% Inverted DC-bus voltage
Vdc = 24;

% Phase voltage limit
Vm_abc = Vdc/2;

% Grid the phase current space
[va, vb, vc] = meshgrid(-Vm_abc:1:Vm_abc);

va = va(:)';
vb = vb(:)';
vc = vc(:)';

% Create vector v_abc
v_abc = [va; vb; vc];

% Transform to alpha-beta
v_abg = K*v_abc;

% Plot the the alpha-beta points
figure;
hold on
axis equal
plot(v_abg(1, :), v_abg(2, :), '.');

% Plot inscribed circle
ang = 0:0.01:2*pi; 
x_circle = 2/sqrt(3)*Vm_abc*cos(ang);
y_circle = 2/sqrt(3)*Vm_abc*sin(ang);
plot(x_circle, y_circle);


%% Plot 2 - the border of the feasible set using voltage vectors
%syms Vdc
Vdc = 24;
vv_abc = Vdc*[-1/2, -1/2, -1/2;
              +1/2, -1/2, -1/2;
              +1/2, +1/2, -1/2;
              -1/2, +1/2, -1/2;
              -1/2, +1/2, +1/2;
              -1/2, -1/2, +1/2;
              +1/2, -1/2, +1/2;
              +1/2, +1/2, +1/2]';
vv_abg = K*vv_abc;
vv_alpha_beta = vv_abg(1:2, :);

border = [vv_alpha_beta(1, 2:7), vv_alpha_beta(1, 2);
          vv_alpha_beta(2, 2:7), vv_alpha_beta(2, 2)];
plot(border(1, :), border(2, :))



%% Plot 3 - plot the feasible region using other form of the constraints (to check validity)
f1 = @(x, y) abs(y);
f2 = @(x, y) abs(y + sqrt(3)*x);
f3 = @(x, y) abs(y - sqrt(3)*x);

% Phase current limit
Vdc = 24;
%Vm = sqrt(3)*vdc/2;

% Grid the DQ current space
[va, vb] = meshgrid(-2*Vdc:.05:2*Vdc);
va = va(:)';
vb = vb(:)';

% Remove points that do not satisfy the conditions
indices = [];
indices = [indices, find(f1(va, vb) > 1/sqrt(3)*Vdc)];
indices = [indices, find(f2(va, vb) > 2/sqrt(3)*Vdc)];
indices = [indices, find(f3(va, vb) > 2/sqrt(3)*Vdc)];
indices = unique(indices);
va(indices) = [];
vb(indices) = [];


%figure;
hold on
axis equal

% Plot borders
% plot(line1(1, :), line1(2, :));
% plot(line2(1, :), line2(2, :))
% plot(line3(1, :), line3(2, :))
% plot(line4(1, :), line4(2, :))
plot(va, vb, '.');
plot(x_circle, y_circle);


%% Plot 4 - verify functions describing the border
Vdc = 24;
f1 = @(x) -sqrt(3)*x + 2/sqrt(3)*Vdc;
f2 = @(x) 1/sqrt(3)*Vdc;
f3 = @(x) +sqrt(3)*x + 2/sqrt(3)*Vdc;
f4 = @(x) -sqrt(3)*x - 2/sqrt(3)*Vdc;
f5 = @(x) -1/sqrt(3)*Vdc;
f6 = @(x) +sqrt(3)*x - 2/sqrt(3)*Vdc;

line1 = [    border(1, 1),     border(1, 2);
         f1(border(1, 1)), f1(border(1, 2))];
line2 = [    border(1, 2),     border(1, 3);
         f2(border(1, 2)), f2(border(1, 3))];
line3 = [    border(1, 3),     border(1, 4);
         f3(border(1, 3)), f3(border(1, 4))];
line4 = [    border(1, 4),     border(1, 5);
         f4(border(1, 4)), f4(border(1, 5))];
line5 = [    border(1, 5),     border(1, 6);
         f5(border(1, 5)), f5(border(1, 6))];
line6 = [    border(1, 6),     border(1, 1);
         f6(border(1, 6)), f6(border(1, 1))];

border_verify = [line1, line2, line3, line4, line5, line6];
plot(border_verify(1, :), border_verify(2, :))



%% Plot 5 - filled feasible region
hfig = figure;
set_figure();
axis equal
hold on
patch('Faces', 1:length(border), 'Vertices', border', 'EdgeColor', 'g', 'FaceColor', 'g', 'FaceAlpha', 0.1);
plot(x_circle, y_circle, '--b');
hleg = legend('Feasible region$\quad\quad\,\,$', '$v_{\alpha}^2 + v_{\beta}^2 = V_{\textrm{max}}^2$', 'Location', 'northeastoutside');
xlabel('$v_{\alpha}$ [V]');
ylabel('$v_{\beta}$ [V]');

% Enlarge figure width by the width of the legend box
enlarge_fig(hfig, hleg);

% Set x-axis limits
ylim([-3/4*Vdc, 3/4*Vdc]);

% Set numbers on axes
set(gca,'xtick', [-20:5:20]);
set(gca,'ytick', [-20:5:20]);


%% Save plot 5

% Save
% file_name = 'constraints2.pdf';
% saveas(gca, file_name);
% system(['pdfcrop ', file_name, ' ', file_name]);


%% Save to border_voltage_alpha_beta
border_voltage_ab = border;
circle_x_voltage_ab = x_circle;
circle_y_voltage_ab = y_circle;


