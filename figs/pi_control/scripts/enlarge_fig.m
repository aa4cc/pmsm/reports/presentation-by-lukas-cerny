function [outputArg1,outputArg2] = enlarge_fig(hfig, hleg)
%enlarge_plot Enlarges figure width by the width of legend box. Meant to be
%used when the legend box is outside the plot either to the right or to the
%left (so all generated plots have the same size).
    
    fig_pos = get(hfig, 'position');
    leg_pos = get(hleg, 'position');
    
    leg_width = fig_pos(3);
    fig_pos(3) = fig_pos(3) + leg_width;
    
    set(hfig, 'position', fig_pos);
    
end

