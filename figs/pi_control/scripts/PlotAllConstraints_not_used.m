
%% Clear workspace
clear all
close all
clc


%% Crete plots
PlotCurrentConstraints;
PlotVoltageConstraints;
PlotCurrentConstraintsDQ;
PlotVoltageConstraintsDQ;
close all

%%
hfig = figure;

subplot(2, 1, 1);
set_figure();
hold on
axis equal
patch('Faces', 1:length(border_current_ab), 'Vertices', border_current_ab', 'EdgeColor', 'g', 'FaceColor', 'g', 'FaceAlpha', 0.1);
plot(circle_x_current_ab, circle_y_current_ab, '--b');
hleg = legend('Feasible region$\quad\quad\,$', '$i_{\alpha}^2 + i_{\beta}^2 = I_{\textrm{max}}$^2', 'Location', 'northeastoutside');
xlabel('$i_{\alpha}$ [A]');
ylabel('$i_{\beta}$ [A]');

% Set axes limits
ylim([-4.5, 4.5])
xlim([-4.5, 4.5])

% Set numbers on axes
set(gca,'xtick', [-6:2:6]);
set(gca,'ytick', [-6:2:6]);



subplot(2, 1, 2);
set_figure();
hold on
axis equal
patch('Faces', 1:length(border_current_dq), 'Vertices', border_current_dq', 'EdgeColor', 'g', 'FaceColor', 'g', 'FaceAlpha', 0.1);
plot(circle_x_current_dq, circle_y_current_dq, '--b');
hleg = legend('App. feasible region', '$i_{\textrm{d}}^2 + i_{\textrm{q}}^2 = I_{\textrm{max}}^2$', 'Location', 'northeastoutside');
xlabel('$i_{\textrm{d}}$ [A]');
ylabel('$i_{\textrm{q}}$ [A]');

% Set axes limits
ylim([-4.5, 4.5])
xlim([-4.5, 4.5])

% Set numbers on axes
set(gca,'xtick', [-6:2:6]);
set(gca,'ytick', [-6:2:6]);


%%


