\documentclass[aspectratio=169]{beamer}

\setbeamercovered{transparent}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{datetime}	
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{siunitx}
\usepackage{multimedia}
\usepackage{amsmath,amssymb,lmodern}
\usepackage{subcaption}
\usepackage{bm}
\usepackage[export]{adjustbox}

\newcommand{\transpose}{\ensuremath{^{\textrm{T}}}}

\mode<presentation> {
	\usetheme{Boadilla}
	\usecolortheme{orchid}
	\setbeamertemplate{navigation symbols}{} 
}

\title[Algorithms for Control of PMSMs]{Algorithms for Advanced Motion Control Using Permanent Magnet Synchronous Motors and Brushless DC Motors}
 
\titlegraphic{\vspace{-3cm}%
\hspace{0.4cm}%
\includegraphics[width=0.25\textwidth]{figs/downloaded/bldc2}%
\hfill%
\includegraphics[width=0.2\textwidth]{figs/downloaded/bldc1}%
\hspace{0.4cm}
}

\author{Lukáš Černý}

\institute[] {
	Faculty of Electrical Engineering\\
	\medskip
	Czech Technical University in Prague\\
}

\newdate{date}{8}{09}{2020}
\date{\displaydate{date}}


\begin{document}



\begin{frame}
	\titlepage
\end{frame}



\begin{frame}
	\frametitle{Motivation}
	\begin{columns}[c]
		\column{.6\textwidth}
			\begin{itemize}
				\vskip-4em
				\item Advantages of PMSMs and BLDC motors
				\begin{itemize}
					\vskip0.5em
					\item Torque-to-size ratio
					\vskip0.5em
					\item Efficiency
				\end{itemize}
				\vskip1em
				\item Advantages of MPC over cascaded PI controller?
			\end{itemize}
		\column{.4\textwidth} 
			\begin{figure}
				\includegraphics[width=0.8\linewidth]{figs/downloaded/doggo1}
			\end{figure}
			\begin{figure}
				\includegraphics[width=0.8\linewidth]{figs/downloaded/liteplacer}
			\end{figure}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Goals}
	\begin{figure}
		\includegraphics[width=0.95\linewidth]{figs/dilci_cile/dilci_cile_en}
	\end{figure}
\end{frame}



\begin{frame}
	\frametitle{PMSM vs. BLDC Motor}
	\begin{columns}[c]
		\column{.45\textwidth}
			\begin{itemize}
				\item Different back EMF waveform
			\end{itemize}
			\vskip1em
			\begin{figure}
				\includegraphics[width=0.8\linewidth]{figs/pmsm_vs_bldc/pmsm_vs_bldc}
			\end{figure}
		\column{.55\textwidth} 
			\begin{figure}
				\includegraphics[height=3cm]{figs/downloaded/book1}
				\includegraphics[height=3cm]{figs/downloaded/book2}
				\includegraphics[height=3cm]{figs/downloaded/book3}
			\end{figure}
			\begin{figure}
				\includegraphics[height=2.5cm]{figs/downloaded/pmsm_inside}
				\includegraphics[height=2.5cm]{figs/downloaded/bldc_inside}
			\end{figure}
	\end{columns}
\end{frame}



\begin{frame}
	\frametitle{Model of PMSM}
	\begin{columns}[c]
		\column{.6\textwidth}
			\begin{figure}
				\includegraphics[width=0.9\linewidth]{figs/model/model_en}
			\end{figure}
		\column{.4\textwidth} 
			\begin{figure}
				\includegraphics[width=0.75\linewidth]{figs/model/pmsm_and_inverter_en}
			\end{figure}
	\end{columns}
\end{frame}





\begin{frame}
	\frametitle{Model of PMSM}
	\begin{columns}[c]
		\column{.6\textwidth}
			\vskip1em
			\begin{itemize}
				\item Linearized model:
				\begin{eqnarray*}
					\dot{x} = A(t)x + B(t)u,
					&&x = \begin{bmatrix}
						i_{\textrm{d}} & i_{\textrm{q}} & \omega & \theta
					\end{bmatrix}^{\textrm{T}}, \\
					&&u = \begin{bmatrix}
						v_{\textrm{d}} & v_{\textrm{q}}
					\end{bmatrix}^{\textrm{T}} \;\textrm{or}\;
					u = \begin{bmatrix}
						u_{\textrm{a}} & u_{\textrm{b}} & u_{\textrm{c}}
					\end{bmatrix}^{\textrm{T}}
				\end{eqnarray*}
				\item Discretization
			\end{itemize}
		\column{.4\textwidth} 
				\begin{figure}
					\includegraphics[width=0.75\linewidth]{figs/model/pmsm_and_inverter_en}
				\end{figure}
	\end{columns}
\end{frame}



\begin{frame}
	\frametitle{Model of PMSM -- Constraints}
	\begin{columns}[c]
		\column{.6\textwidth}
			\begin{itemize}
				%\pause
				\vskip-4em
				\item Constraints on currents
				\begin{align*}
					i_{\textrm{d}}^2 + i_{\textrm{q}}^2 \leq I_{\textrm{max}}
				\end{align*}
				%\pause
				\item Constraints on voltages
				\begin{align*}
					v_{\textrm{d}}^2 + v_{\textrm{q}}^2 \leq V_{\textrm{max}}
				\end{align*}
				%\pause
				\item Approximated by linear constraints
			\end{itemize}
		\column{.4\textwidth} 
			\begin{figure}
			\vskip-1em
				%\visible<2->{
					\includegraphics[width=0.98\linewidth]{figs/constraints/constraints3}
				%}
			\end{figure}
			\vskip-1em
			\begin{figure}
				%\visible<3->{ %width is 0.86*width of the first figure
					\includegraphics[width=1.0\linewidth]{figs/constraints/constraints4}
				%}
			\end{figure} 
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Algorithms for Control of PMSM}
	\begin{columns}[c]
		\column{.38\textwidth}
			\begin{itemize}
				\pause
				\item With PWM
				\begin{itemize}
					\vskip0.5em
					\item PI regulator
					\vskip0.5em
					\item Continuous-control-set MPC (CCS-MPC)
				\end{itemize}
				\vskip2.5em
				\pause
				\item Without PWM
				\begin{itemize}
					\vskip0.5em
					\item Finite-control-set MPC (FCS-MPC)
				\end{itemize}
			\end{itemize}
		\column{.62\textwidth} 
			\begin{figure}
				\visible<2->{
					\includegraphics[width=0.98\linewidth]{figs/control_structures/control_structure1}
				}
			\end{figure}
			\vskip-0.5em
			\begin{figure}
				\visible<3>{ %width is 0.86*width of the first figure
					\includegraphics[width=0.842\linewidth]{figs/control_structures/control_structure2}
				}
			\end{figure}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Algorithms for Control of PMSM}
	\begin{columns}[c]
		\column{.45\textwidth}
			\begin{itemize}
				\pause
				\item Cascaded structure
				\vskip6em
				\pause
				\item Centralized structure
			\end{itemize}
		\column{.55\textwidth} 
			\begin{figure}
				\visible<2->{
					\includegraphics[width=1\linewidth]{figs/control_structures/control_structure4}
				}
			\end{figure}
			\vskip-0.5em
			\begin{figure}
				\visible<3>{ %width is 0.86*width of the first figure
					\includegraphics[width=0.6\linewidth]{figs/control_structures/control_structure3}
				}
			\end{figure}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{PI Regulator}
	\begin{columns}[c]
		\column{.4\textwidth}
			\begin{itemize}
				\item Cascaded structure
				\vskip1em
				\item Constraints implemented using saturation
				\begin{align*}
					|i_{\textrm{d}}| &\leq I_{\textrm{max}}\\
					|i_{\textrm{q}}| &\leq I_{\textrm{max}}\\ 
					|v_{\textrm{d}}| &\leq V_{\textrm{max}}\\
					|v_{\textrm{q}}| &\leq V_{\textrm{max}}
				\end{align*}
			\end{itemize}
		\column{.6\textwidth} 
			\begin{figure}
					\includegraphics[width=1.0\linewidth]{figs/pi_control/pi_structure}
			\end{figure} 
			\begin{figure}
			%\vskip-1em
				%\visible<2->{
					\includegraphics[width=0.45\linewidth]{figs/pi_control/constraints3}
					\includegraphics[width=0.5\linewidth]{figs/pi_control/constraints4}
				%}
			\end{figure}
	\end{columns}
\end{frame}

\begin{frame}
	\frametitle{Continuous-Control-Set MPC (CCS-MPC)}
	\begin{columns}[c]
		\column{.55\textwidth}
			\begin{itemize}
				\item QP problem
				\vskip1em
				\item Cascaded or centralized
				\vskip1em
				\item Solution 
				\begin{itemize}
					\vskip0.5em
					\item Proximal gradient method + warm start
				\end{itemize}
			\end{itemize}
		\column{.45\textwidth} 
			\begin{figure}
				%\visible<3>{ %width is 0.86*width of the first figure
					\includegraphics[width=0.75\linewidth]{figs/ccsmpc/ccsmpc_en}
				%}
			\end{figure}
	\end{columns}
\end{frame}

\begin{frame}
	\frametitle{Finite-Control-Set MPC (FCS-MPC)}
	\begin{columns}[c]
		\column{.55\textwidth}
			\begin{itemize}
				\item Integer QP problem
				\vskip1em
				\item Values of manipulated variables are from a finite set
				\vskip1em
				\item Solution
				\begin{itemize}
					\item DFS + cutting
					\vskip1em
					\item Initial guess -- solution from the last sample time
					\vskip1em
					\item Reformulation -- change of the branching factor to 2
				\end{itemize}
			\end{itemize}
		\column{.45\textwidth} 
			\begin{figure}
				%\visible<3>{ %width is 0.86*width of the first figure
					\includegraphics[width=0.75\linewidth]{figs/fcsmpc/fcsmpc_en}
				%}
			\end{figure}
				\vskip-1em
			\begin{figure}
				%\visible<3>{ %width is 0.86*width of the first figure
					\includegraphics[width=0.5\linewidth]{figs/fcsmpc/fcsmpc_tree}
				%}
			\end{figure}
	\end{columns}
\end{frame}

\begin{frame}
	\frametitle{Simulation -- Torque Tracking (Current Tracking with $i_{\textrm{dref}}=0$)}
	
\begin{figure}[tb]
	%\begin{center}
		\begin{subfigure}[b]{0.25\textwidth}
			%\centering
  			\includegraphics[width=1.0\textwidth]{figs/torque_tracking/pid_q_current.pdf}
		\end{subfigure}
		\begin{subfigure}[b]{0.25\textwidth}
			%\centering
  			\includegraphics[width=1.0\textwidth]{figs/torque_tracking/ccsmpc_q_current.pdf}
		\end{subfigure}
		\begin{subfigure}[b]{0.25\textwidth}
			%\centering
 	 		\includegraphics[width=1.0\textwidth]{figs/torque_tracking/fcsmpc_q_current.pdf}
		\end{subfigure}
		\par\bigskip
		\begin{subfigure}[b]{0.25\textwidth}
			\centering
  			\includegraphics[width=1.0\textwidth]{figs/torque_tracking/pid_d_current.pdf}
  			\subcaption{PI regulator\\{\tiny($f_{\textrm{s}}=25\,\textrm{kHz}$)\qquad\qquad\qquad}}
		\end{subfigure}
		\begin{subfigure}[b]{0.25\textwidth}
			\centering
  			\includegraphics[width=1.0\textwidth]{figs/torque_tracking/ccsmpc_d_current.pdf}
  			\subcaption{CCS-MPC\\{\tiny($f_{\textrm{s}}=25\,\textrm{kHz}$, $N = 5$)}}
		\end{subfigure}
		\begin{subfigure}[b]{0.25\textwidth}
			\centering
 	 		\includegraphics[width=1.0\textwidth]{figs/torque_tracking/fcsmpc_d_current.pdf}
  			\subcaption{FCS-MPC\\{\tiny($f_{\textrm{s}}=500\,\textrm{kHz}$, $N = 6$)}}
		\end{subfigure}
	%\end{center}
\end{figure}
\end{frame}


\begin{frame}
	\frametitle{Simulation -- Torque Tracking (Current Tracking with $i_{\textrm{dref}}=0$)}
	
\begin{figure}[tb]
	%\begin{center}
		\begin{subfigure}[b]{0.25\textwidth}
			\centering
  			\includegraphics[width=1.0\textwidth]{figs/torque_tracking/fcsmpc_smooth_q_current.pdf}
  			\subcaption{FCS-MPC 1\\{\tiny(low penalization of switching)\qquad\qquad\qquad}}
		\end{subfigure}
		\begin{subfigure}[b]{0.25\textwidth}
			\centering
  			\includegraphics[width=1.0\textwidth]{figs/torque_tracking/fcsmpc_regular_q_current.pdf}
  			\subcaption{FCS-MPC 2\\{\tiny(medium penalization of switching)}}
		\end{subfigure}
		\begin{subfigure}[b]{0.25\textwidth}
			\centering
 	 		\includegraphics[width=1.0\textwidth]{figs/torque_tracking/fcsmpc_ripple_q_current.pdf}
  			\subcaption{FCS-MPC 3\\{\tiny(high penalization of switching)}}
		\end{subfigure}
	%\end{center}
\end{figure}
\end{frame}


\begin{frame}
	\frametitle{Simulation -- Velocity Tracking}
	
\begin{figure}[tb]
	%\begin{center}
		\begin{subfigure}[b]{0.225\textwidth}
			%\centering
  			\includegraphics[width=1.0\textwidth]{figs/velocity_tracking/pid_omega.pdf}
		\end{subfigure}
		\begin{subfigure}[b]{0.225\textwidth}
			%\centering
  			\includegraphics[width=1.0\textwidth]{figs/velocity_tracking/ccsmpc_omega.pdf}
		\end{subfigure}
		\begin{subfigure}[b]{0.225\textwidth}
			%\centering
 	 		\includegraphics[width=1.0\textwidth]{figs/velocity_tracking/ccsmpccas_omega.pdf}
		\end{subfigure}
		\begin{subfigure}[b]{0.225\textwidth}
			\centering No data \vspace{3em}
		\end{subfigure}
		\par\bigskip
		\begin{subfigure}[b]{0.225\textwidth}
			%\centering
  			\includegraphics[width=1.0\textwidth]{figs/velocity_tracking/pid_iq.pdf}
  			\subcaption{PI regulator\\{\tiny($f_{\textrm{s}1}=25\,\textrm{kHz}$, $f_{\textrm{s}2}=1\,\textrm{kHz}$)}\\ \qquad}
		\end{subfigure}
		\begin{subfigure}[b]{0.225\textwidth}
			%\centering
  			\includegraphics[width=1.0\textwidth]{figs/velocity_tracking/ccsmpc_iq.pdf}
  			\subcaption{CCS-MPC\\{\tiny($f_{\textrm{s}}=25\,\textrm{kHz}$, $N = 5$)}\\ \qquad}
		\end{subfigure}
		\begin{subfigure}[b]{0.225\textwidth}
			%\centering
 	 		\includegraphics[width=1.0\textwidth]{figs/velocity_tracking/ccsmpccas_iq.pdf}
  			\subcaption{Kaskádní CCS-MPC\\{\tiny($f_{\textrm{s1}}=25\,\textrm{kHz}$, $N_1 = 5$}\\{\tiny $f_{\textrm{s2}}=1\,\textrm{kHz}$, $N_2 = 5$)}}
		\end{subfigure}
		\begin{subfigure}[b]{0.225\textwidth}
			\centering No data \vspace{3em}
  			\subcaption{FCS-MPC\\{\tiny($f_{\textrm{s1}}=500\,\textrm{kHz}$, $N = 5$)}\\ \qquad}
		\end{subfigure}
	%\end{center}
\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Simulation -- Position Tracking}
	\begin{itemize}
	\item Stabilized only by cascaded controllers
	\end{itemize}
	\begin{figure}[tb]
		\begin{center}
			\includegraphics[width=0.4\textwidth]{figs/position_tracking/pid_theta.pdf}
		\end{center}
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{Hardware Implementation}
	\begin{columns}[c]
		\column{.6\textwidth}
			\begin{itemize}
				\item ZedBoard + AD-FMCMOTCON2-EBZ inverter
				\begin{itemize}
					\vskip0.5em
					\item ARM Processor + FPGA
					\vskip0.5em
					\item Control of two motors
				\end{itemize}
			\end{itemize}
		\column{.4\textwidth} 
			\begin{figure}
				\includegraphics[width=0.9\linewidth]{figs/foto/foto}
			\end{figure}
	\end{columns}
				\vskip0.5em
			\begin{figure}
				\includegraphics[width=0.8\linewidth]{figs/hardware/hardware_en}
			\end{figure}
\end{frame}


\begin{frame}
	\frametitle{Conclusion}
	\begin{figure}
		\includegraphics[width=0.95\linewidth]{figs/dilci_cile/zaver_en}
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{Figure References}
	{
	\tiny
	\begin{itemize}
		\item \url{https://imgaz2.staticbg.com/thumb/large/oaupload/banggood/images/8F/1A/b42c8504-6aea-4415-b1e4-201d5ac0b9ca.jpg}
		\vskip1em
		\item \url{https://images-na.ssl-images-amazon.com/images/I/51kzqtSwbPL._AC_SL1000_.jpg}
		\vskip1em
		\item \url{https://cdn.hackaday.io/images/8016611546148663631.jpg}
		\vskip1em
		\item \url{https://www.robotics-3d.com/5046-large_default/liteplacer-the-prototyping-pick-and-place-machine.jpg}
		\vskip1em
		\item \url{https://d3i71xaburhd42.cloudfront.net/11a76481554a470f8a790a96b9d3530d7d71045f/2-Figure1-1.png}
		\vskip1em
		\item \url{http://rcduniya.com/product/360kv-bldc-motor}
		\vskip1em
		\item \url{https://images-na.ssl-images-amazon.com/images/I/41xcc5FK+IL._SX310_BO1,204,203,200_.jpg}
		\vskip1em
		\item \url{https://images-na.ssl-images-amazon.com/images/I/71zqfWfVEBL.jpg}
		\vskip1em
		\item \url{https://images-na.ssl-images-amazon.com/images/I/41IhTxazsyL._SX343_BO1,204,203,200_.jpg}
	\end{itemize}
	}
\end{frame}



\end{document} 
